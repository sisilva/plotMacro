// WZplots.C
// global user variables and methods for WZplots 
// Alexander Law (atlaw@ucsc.edu)
// University of California, Santa Cruz
// June 2015

#include <iostream>
#include "TROOT.h"
#include "TStyle.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TColor.h" // kColors
#include "THStack.h"
#include "TH1.h"
#include "TH1F.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TAttText.h"
#include "TLatex.h"
#include "TGaxis.h"
#include "TString.h"
#include "TLine.h"
#include "TGraphAsymmErrors.h"
#include "TF1.h" //Simao
#include "cmath" //Simao
#include "TFile.h" //Simao
#include <string>
#include <vector>
#include <algorithm> // std::find

// I don't know a better way to get 
// these functions ... 
#include "AtlasUtils.h"

// Global variables! In the header! Ahhh!
/*****************************************************************/
/*****************************************************************/
/*   THESE ARE DEFAULTS & DECLARATIONS. DON'T EDIT VALUES HERE.  */
/*****************************************************************/
/*****************************************************************/

UInt_t   textsize = 25; // in pixel 
UInt_t   fontNumber = 43; // fixed-pixel-size Helvetica with no italics or bold

// should have no influence because of fixed text and symbol size
// text size is fixed ...
// how to fix the symbol size?
Double_t g_legend_width = 0.2;
UInt_t   legendTextSize = 20;
Double_t g_legend_height_per_entry = 0.06; // fraction of the pad?
Double_t ratioPadFraction = 0.3;
double ratio_leftbottommargin_rightmargin = 2.3;

std::string outFileName = "DEFAULTPLOTOUTFILENAME.pdf";

bool   useAsymmetricErrors = false;
int    errorBandFillStyle   = 3001; // 3345 black hatching w/ transparent background - displays well in all common image formats.
double errorBandLineWeight  = 0.75;
double errorBandLineSpacing = 1;
int    errorBandColor       = kBlack;
double GeV = 1000;
double xScale = 1;  // scaling for x-axis, mostly for scaling MeV histograms to GeV for display
double xmin = 0;    // x-axis range minimum (after rescaling)
double xmax = 100;  // x-axis range maximum (after rescaling)
bool logy = false;
bool logx = false;
bool   yRangeUserDefined    = false;
double ymin                 = 0.001; // not zero in case of log plot
double ymax                 = 1;
// bool ratio = true;
bool doRatio = true;
bool fakeData = false; // 
double yminRatio = 0.69;
double ymaxRatio = 1.31;
unsigned int nDivisionsRatioYaxis = 6;
// Fit ratio to a order 0 polynomial
bool doFitP0 = false;
double fitXmin = 0.0;
double fitXmax = 1.0;

double legend_x1 = 0.60;
double legend_y1 = 0.45;
double titleX = 0.15; // unused, preserved for back-compatibility
double titleY = 0.80; // unused, preserved for back-compatibility
double atlasX = 0.55;
double atlasY = 0.78;

bool labelATLAS = true;
bool labelStatus = true;
bool labelStatus2 = false;
bool labelLumi = true;

std::string titleLaTeX = "DEFAULTTITLE";
TString yAxisTitle = "Entries";
TString xAxisUnitsString = "XUNITS";
std::string lumiString = "85 pb^{-1}";       // This will be parsed as LaTeX.
std::string statusString = "Internal";       // "Internal", and "Preliminary" are the official choices
std::string status2String = "SIMSTATUS";    // "Simulation," if the data histogram shows MC. 
std::string xAxisTitle = "XAXIS";     // this is parsed as TLaTeX (ROOT's busted LaTeX imitations) _{xx} is subscript, ^{xx} is superscript
std::string yAxisTitleRatioPlot = "Data/Pred."; // 
std::string errorBandLegendEntry = "MC Stat. #oplus Syst. Unc."; 
bool errorBandLegendEntryTwoLines = false;
std::string errorBandLegendEntryLineTwo = "#oplus Syst. Unc.";
std::string binWidthPrecisionFormatString = "%.0f"; // printf format, " %.{N}f", e.g., "%.0f", where N is the number of decimal points - usually zero, 1 or 2 for eta plots

std::vector<TH1F*> legendExcludes; // list of sample histograms to exclude form the legend : addHistToListNoLegend
std::vector<TH1F*> histList; 
TH1F* h_data = NULL; // point this to your data histogram, if you have it. 
TH1F* h_sys = NULL;  // point this to your systematics histogram, if you have it. 
TH1F* h_err_up = NULL; // histogram for upper asymm. errors
TH1F* h_err_dn = NULL; // histogram for lower asymm. errors

// forward-declarations
TGraphAsymmErrors* buildAsymmetricErrors(TH1F* stack, TH1F* errup, TH1F* errdn, bool doratio);
TH1F* scaleXaxis(TH1F* hist, double scale);
TH1F* buildStackSum();
TH1F* buildData();
TH1F* buildRatio(TH1F* data, TH1F* sum); 
TCanvas* buildCanvas();
TH1F* buildMainCanvas();
TPad* buildMainPad(TCanvas* c);
TPad* buildRatioPad(TCanvas* c);
THStack* buildStack(TPad* mainpad);
TH1F* buildSystematicBand();
TH1F* buildSystematicBandRatio(TH1F* stacksum);
void  configureRatioFrameAxes(TH1F* frame); 
TString buildBinSizeString(TH1F* h);
void removeXaxis(THStack* s);
TLegend* buildLegend(TH1F* data, TH1F* sys, TGraphAsymmErrors* = NULL);
void ConfigureLegend(TLegend* legend, std::string header = "", int nheaderlines=1);
void drawLumiAndStatus();
void addHistToList(TH1F* hist, std::string processName, bool legend);
// trivial overload to invoke with default legend choice
void addHistToList(TH1F* hist, std::string processName);
// this is a trivial overload to preserve back-compatibility with preexisting templates.
void addHistToList(std::vector<TH1F*>* s, TH1F* h, std::string name, double scale);
void addHistToListNoLegend(TH1F* hist, std::string processName);
void addSignalToStack(TH1F* hist, std::string processName);
// this is a trivial overload to preserve back-compatibility with preesixsting templates.
void addSignalToStack(std::vector<TH1F*>* s, TH1F* h, std::string name, double scale);
void printPlot(std::string modName, bool asymmErrors=false);
void SetControlPlotStyle(Double_t ratio_frac=0.3, Double_t margin=0.05, Int_t font=43, Double_t size=textsize);
std::string getProcessLegendEntry(std::string proc);


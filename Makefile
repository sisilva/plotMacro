LIBS = `root-config --libs`
CXXFLAGS = `root-config --cflags`


WZplotsTemplateMod.exe: WZplotsTemplateMod.C libWZplots.so libAtlasStyle.so libAtlasUtils.so
		g++ -g -Wall -o WZplotsTemplateMod.exe WZplotsTemplateMod.C -L. -lWZplots -lAtlasStyle -lAtlasUtils $(LIBS) $(CXXFLAGS)

libWZplots.so: WZplots.C WZplots.h
	g++ -g -Wall -shared -fPIC -o libWZplots.so WZplots.C AtlasUtils.C $(CXXFLAGS) 

libAtlasStyle.so: AtlasStyle.C AtlasStyle.h
	g++ -g -Wall -shared -fPIC -o libAtlasStyle.so AtlasStyle.C $(CXXFLAGS)

libAtlasUtils.so: AtlasUtils.C AtlasUtils.h
	g++ -g -Wall -shared -fPIC -o libAtlasUtils.so AtlasUtils.C $(CXXFLAGS)

.PHONY: clean
clean:
	rm -f lib*.so WZplotsTemplateMod.exe
